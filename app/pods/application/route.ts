import { action } from '@ember/object';
import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import FastbootService from 'ember-cli-fastboot/services/fastboot';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';
import SessionService from 'ember-simple-auth/services/session';
import PageLayout from 'project-setup-test/mixins/page-layout';
import CurrentUserService from 'project-setup-test/services/current-user';

export default class Application extends PageLayout(Route.extend(ApplicationRouteMixin)) {
    @service fastboot!: FastbootService;
    @service currentUser!: CurrentUserService;
    @service session!: SessionService;

    async beforeModel() {
        //existing beforeModel() code...
        if (this.session.isAuthenticated) {
            try {
                await this.currentUser.load();
            } catch (err) {
                this.replaceWith('five-hundred');
            }
        }
    }

    async sessionAuthenticated() {
        //get the current user's model before transitioning from the login page
        const currentUser = await this.currentUser.load();
        //@ts-ignore TODO we need a way to inform TS about class members coming from Ember-style mixins
        super.sessionAuthenticated(...arguments);
        return currentUser;
    }

    @action
    error(error?: any) {
        if (this.fastboot.isFastBoot) {
            this.fastboot.response.statusCode = error?.errors?.firstObject?.status ?? 200;
        }

        if (error?.errors?.length > 0) {
            const status = error.errors.firstObject.status;
            if (status === '403') {
                this.replaceWith('four-oh-three');
                //marks error as being handled
                return false;
            } else if (status === '401') {
                this.replaceWith('login');
                //marks error as being handled
                return false;
            } else if (status === '404') {
                this.replaceWith('four-oh-four', '404');
                //marks error as being handled
                return false;
            }
        }

        return true;
    }
}
