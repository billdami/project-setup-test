import DS from 'ember-data';
import RESTSerializer from 'ember-data/serializers/rest';

export default class ApplicationSerializer extends RESTSerializer {
    /**
     * Sets the root key name for JSON payload objects
     * @return {String}
     */
    payloadKeyFromModelName() {
        return 'data';
    }

    /**
     * Forces PUT requests to include the model "id" in the JSON payload
     * @param  {DS.snapshot} snapshot
     * @param  {Object} options
     * @return {Object}
     */
    serialize(snapshot: DS.Snapshot, options: any) {
        if (!options) {
            options = {};
        }

        //include the record ID in the request body for PUTs, ect
        options.includeId = true;
        return super.serialize(snapshot, options);
    }
}

declare module 'ember-data/types/registries/serializer' {
    export default interface SerializerRegistry {
        application: ApplicationSerializer;
    }
}
