import { computed, get, set } from '@ember/object';
import { inject as service } from '@ember/service';
import { dasherize } from '@ember/string';
import FastbootService from 'ember-cli-fastboot/services/fastboot';
import RESTAdapter from 'ember-data/adapters/rest';
import AdapterFetch from 'ember-fetch/mixins/adapter-fetch';
import DataAdapterMixin from 'ember-simple-auth/mixins/data-adapter-mixin';
import SessionService from 'ember-simple-auth/services/session';
import ENV from 'project-setup-test/config/environment';
import { reject } from 'rsvp';

export default class Application extends RESTAdapter.extend(AdapterFetch, DataAdapterMixin) {
    @service session!: SessionService;
    @service fastboot!: FastbootService;
    // uncomment when using the @gavant/ember-app-version-update addon
    // TODO this needs typings (ETA TBD in an addon update)
    // @service versionUpdate;
    host = ENV.RESTAPI;
    authorizer = 'authorizer:oauth2';
    invalidStatusCodes = [400, 403, 422, 500, 503];

    /**
     * When using ember-fetch with ember-simple-auth, authorization headers must be manually set
     * @return {Object}
     */
    @computed('session.{isAuthenticated,data.authenticated.access_token}')
    get headers() {
        const headers = {} as any;
        if (this.session.isAuthenticated) {
            const { access_token } = this.session.data!.authenticated;
            headers['Authorization'] = `Bearer ${access_token}`;
        }

        return headers;
    }

    /**
     * silences ember-simple-auth authorizer deprecation warning
     * https://github.com/simplabs/ember-simple-auth#deprecation-of-authorizers
     * @param  {XMLHttpRequest} xhr
     * @return {Void}
     */
    authorize(xhr: XMLHttpRequest) {
        const { access_token } = this.session.data!.authenticated;
        xhr.setRequestHeader('Authorization', `Bearer ${access_token}`);
    }

    /**
     * returns true if the status code is defined as "invalid"
     * @param  {Number}  status
     * @return {Boolean}
     */
    isInvalid(status: number) {
        return this.invalidStatusCodes.includes(status);
    }

    /**
     * Handles unauthenticated requests (logs the user out)
     * @param  {Number} status
     * @param  {Object} headers
     * @return {Object}
     */
    handleResponse(status: number, headers: {}, payload: {}, requestData: {}) {
        // uncomment when using the @gavant/ember-app-version-update addon
        // this.versionUpdate.checkResponseHeaders(headers);

        if (status === 401) {
            if (this.session.isAuthenticated) {
                this.session.invalidate();
                return reject();
            } else {
                this.browserRedirect(ENV['ember-simple-auth'].authenticationRoute);
                return {};
            }
        }

        return super.handleResponse(status, headers, payload, requestData);
    }

    /**
     * Safely redirects to a new URL in both FastBoot and client-side environments
     * @param  {String}  url
     * @param  {Number}  [statusCode=307]
     * @param  {Boolean} [replace=false]
     * @return {Void}
     */
    browserRedirect(url: string, statusCode = 307, replace = false) {
        if (this.fastboot.isFastBoot) {
            //avoid redirect loops
            if (this.fastboot.request.path !== url) {
                this.fastboot.response.statusCode = statusCode;
                this.fastboot.response.headers.set('Location', url);
            }
        } else if (replace) {
            window.location.replace(url);
        } else {
            window.location.href = url;
        }
    }

    /**
     * adds query parameter support for single resource requests
     * usage: store.findRecord('type', 123, {adapterOptions: {query: {foo: 'bar'}}});
     * @see https://github.com/emberjs/data/issues/3596#issuecomment-126604014
     *
     * @param  {String} url
     * @param  {DS.Snapshot} snapshot
     * @return {String}
     */
    appendQueryParams(url: string, snapshot: any) {
        const query = get(snapshot, 'adapterOptions.query');

        if (query) {
            //TODO we need to make this more robust, to handle nested/array keys etc
            const queryStr = Object.keys(query)
                .map((k) => `${encodeURIComponent(k)}=${encodeURIComponent(query[k])}`)
                .join('&');
            url += `?${queryStr}`;
        }

        return url;
    }

    /**
     * Customize the model endpoint URLs to use dasherized format
     * @param {any} modelName
     * @return {String}
     */
    pathForType(modelName: any) {
        let pathName = super.pathForType(modelName);
        return dasherize(pathName);
    }

    /**
     * Customize the options passed to the AJAX request
     * @param {string} url
     * @param {string} type
     * @param {(object | undefined)} [options]
     * @return {object}
     */
    ajaxOptions(url: string, type: string, options?: object | undefined) {
        let hash = super.ajaxOptions(url, type, options) as any;
        if (!hash.xhrFields) {
            set(hash, 'xhrFields', {});
        }

        set(hash, 'xhrFields.withCredentials', true);
        return hash;
    }
}

declare module 'ember-data/types/registries/adapter' {
    export default interface AdapterRegistry {
        application: Application;
    }
}
