import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
import SessionService from 'ember-simple-auth/services/session';

export default class Dashboard extends Route.extend(AuthenticatedRouteMixin) {
    @service session!: SessionService;
    // normal class body definition here
    beforeModel(transition) {
        console.log('before model!', this.session.isAuthenticated);
        super.beforeModel(transition);
    }
}
