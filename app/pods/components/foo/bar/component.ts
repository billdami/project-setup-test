import {action} from '@ember/object';
import Component from '@glimmer/component';
import {tracked} from '@glimmer/tracking';

interface FooBarArgs {
    bar?: string;
}

export default class FooBar extends Component<FooBarArgs> {
    @tracked foo: number = 0;

    @action
    increment() {
        this.foo = this.foo + 1;
    }
}
