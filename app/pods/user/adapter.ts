import ModelRegistry from 'ember-data/types/registries/model';
import ApplicationAdapter from 'project-setup-test/pods/application/adapter';

export default class User extends ApplicationAdapter {
    /**
     * Customize the URL for querying for the current user record
     * @param {Object} query
     * @param {String} modelName
     * @returns {String}
     */
    urlForQueryRecord<K extends keyof ModelRegistry>(query: { me?: boolean }, modelName: K): string {
        if (query && query.me) {
            const baseUrl = this.buildURL(modelName);
            return `${baseUrl}/me`;
        } else {
            return super.urlForQueryRecord(query, modelName);
        }
    }

    /**
     * Invoked by the store to make AJAX requests
     * @param {String} url
     * @param {String} type
     * @param {Object} options
     * @returns {Promise}
     */
    ajax(url: string, type: string, options: any) {
        //manually remove the "me" query param from the request
        //as we are overriding the URL for that query in urlForQueryRecord() above
        if (options && options.data && options.data.me === true) {
            delete options.data.me;
        }

        return super.ajax(url, type, options);
    }
}

declare module 'ember-data/types/registries/adapter' {
    export default interface AdapterRegistry {
        user: User;
    }
}
