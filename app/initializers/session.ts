import Application from '@ember/application';

export function initialize(application: Application) {
    ['component', 'controller', 'model', 'route', 'view', 'ability'].forEach((type) =>
        application.inject(type, 'session', 'service:session')
    );
}

export default {
    name: 'session',
    after: 'ember-simple-auth',
    initialize
};
