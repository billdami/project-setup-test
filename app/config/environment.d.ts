export default config;

/**
 * Type declarations for
 *    import config from './config/environment'
 *
 * For now these need to be managed by the developer
 * since different ember addons can materialize new entries.
 */
declare const config: {
    environment: any;
    modulePrefix: string;
    podModulePrefix: string;
    locationType: string;
    rootURL: string;
    RESTAPI: string;
    'ember-simple-auth': {
        authenticationRoute: string;
        routeIfAlreadyAuthenticated: string;
        routeAfterAuthentication: string;
    };
    'simple-auth-oauth2': {
        serverTokenEndpoint: string;
    };
};
