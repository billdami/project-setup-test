import { isArray } from '@ember/array';
import { assign, merge } from '@ember/polyfills';
import { run } from '@ember/runloop';
import { isEmpty } from '@ember/utils';
import Authenticator from 'ember-simple-auth/authenticators/oauth2-password-grant';
import fetch from 'fetch';
import ENV from 'project-setup-test/config/environment';
import RSVP from 'rsvp';

interface AuthenticateData {
    scope?: string;
    client_id: string;
    grant_type: string;
    username: string;
    password: string;
}

export default class OAuth2Authenticator extends Authenticator {
    // uncomment when using the @gavant/ember-app-version-update addon
    // TODO this needs typings (ETA TBD in an addon update)
    // @service versionUpdate;
    // uncomment this if gavant-ember-websockets is being used in this app
    // clientIdentity: service(),
    serverTokenEndpoint = ENV['simple-auth-oauth2'].serverTokenEndpoint;

    /**
     * oauth authentication
     * reimplementing authenticate to add client_id to request data
     * @see //https://github.com/simplabs/ember-simple-auth/blob/1.8.2/addon/authenticators/oauth2-password-grant.js#L255
     *
     * @param  {String} identification
     * @param  {String} password
     * @param  {Array|String}  [scope=[]]
     * @param  {Object} [headers={}]
     * @return {Promise}
     */
    authenticate(identification: string, password: string, scope: string[] | string = [], headers = {}) {
        return new RSVP.Promise((resolve, reject) => {
            const data: AuthenticateData = {
                client_id: 'web',
                grant_type: 'password',
                username: identification,
                password
            };
            const serverTokenEndpoint = this.serverTokenEndpoint;
            const useResponse = this.rejectWithResponse;
            const scopesString = ((!isArray(scope) ? [scope] : scope) as string[]).join(' ');
            if (!isEmpty(scopesString)) {
                data.scope = scopesString;
            }
            this.makeRequest(serverTokenEndpoint, data, headers).then(
                (response: any) => {
                    run(() => {
                        if (!this._validate(response)) {
                            reject('access_token is missing in server response');
                        }

                        const expiresAt = this._absolutizeExpirationTime(response['expires_in']);
                        this._scheduleAccessTokenRefresh(response['expires_in'], expiresAt, response['refresh_token']);
                        if (!isEmpty(expiresAt)) {
                            response = assign(response, { expires_at: expiresAt });
                        }

                        resolve(response);
                    });
                },
                (response) => {
                    run(null, reject, useResponse ? response : response.responseJSON || response.responseText);
                }
            );
        });
    }

    /**
     * reimplementing makeRequest so that we have access to the entire response object (for checking headers for new app version)
     * @see https://github.com/simplabs/ember-simple-auth/blob/1.8.2/addon/authenticators/oauth2-password-grant.js#L336
     *
     * @param  {String} url
     * @param  {Object} data
     * @param  {Object} [headers={}]
     * @return {Promise}
     */
    makeRequest(url: string, data: any, headers: any = {}) {
        // uncomment this if gavant-ember-websockets is being used in this app
        // assign(headers, this.clientIdentity.uuidHeader);
        headers['Content-Type'] = 'application/x-www-form-urlencoded';

        const body = Object.keys(data)
            .map((key) => {
                return `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`;
            })
            .join('&');

        const options = {
            body,
            headers,
            method: 'POST'
        };

        const clientIdHeader = this._clientIdHeader;
        if (!isEmpty(clientIdHeader)) {
            merge(options.headers, clientIdHeader!);
        }

        return new RSVP.Promise((resolve, reject) => {
            fetch(url, options)
                .then((response) => {
                    //change for gavant-ember-app-version-update
                    // uncomment when using the @gavant/ember-app-version-update addon
                    // this.versionUpdate.checkResponseHeaders(response.headers.map);

                    response.text().then((text) => {
                        try {
                            let json = JSON.parse(text);
                            if (!response.ok) {
                                //@ts-ignore ember-simple-auth adds a non-standard property being added to a Response object
                                response.responseJSON = json;
                                reject(response);
                            } else {
                                //modify resolved value to handle gavant's custom oauth JSON response
                                resolve(json.oAuth2AccessToken);
                            }
                        } catch (SyntaxError) {
                            //@ts-ignore ember-simple-auth adds a non-standard property being added to a Response object
                            response.responseText = text;
                            reject(response);
                        }
                    });
                })
                .catch(reject);
        });
    }
}
