import Service, { inject as service } from '@ember/service';
import { tracked } from '@glimmer/tracking';
import FastbootService from 'ember-cli-fastboot/services/fastboot';
import DS from 'ember-data';
import SessionService from 'ember-simple-auth/services/session';
import User from 'project-setup-test/pods/user/model';
import AjaxService from 'project-setup-test/services/ajax';

export default class CurrentUserService extends Service {
    @service store!: DS.Store;
    @service session!: SessionService;
    @service ajax!: AjaxService;
    @service fastboot!: FastbootService;

    @tracked user: User | null = null;

    /**
     * loads the current user from the API
     * @return {Promise}
     */
    load() {
        return this.fetchUser();
    }

    /**
     * Refreshes the current user if logged in
     * @return {Promise}
     */
    refresh() {
        //only attempt to refresh the user if there is a logged in user
        if (this.session.isAuthenticated) {
            return this.fetchUser();
        }
    }

    /**
     * Fetches the current user model from the /users/me
     * @return {Promise}
     */
    async fetchUser() {
        const user: User = await this.store.queryRecord('user', { me: true });
        //TODO can we improve this at all so we dont need to ts-ignore it?
        //@ts-ignore allow setting a non-standard property `user` on the session service instance
        this.session.set('user', user);
        this.user = user;
        return user;
    }
}
