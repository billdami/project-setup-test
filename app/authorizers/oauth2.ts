import OAuth2Bearer from 'ember-simple-auth/authorizers/oauth2-bearer';

export default class OAuth2Authorizer extends OAuth2Bearer {}
