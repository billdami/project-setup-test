import CookieStore from 'ember-simple-auth/session-stores/cookie';

export default class ApplicationCookieStore extends CookieStore {}
