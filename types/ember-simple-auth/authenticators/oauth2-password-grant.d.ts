declare module 'ember-simple-auth/authenticators/oauth2-password-grant' {
    export default class OAuth2PasswordGrant {
        serverTokenEndpoint: string;
        rejectWithResponse: boolean;
        _clientIdHeader: {} | undefined;
        authenticate(identification: string, password: string, scope: string[] | string, headers: {}): Promise<any>;
        makeRequest(url: string, data: {}, headers: {}): Promise<any>;
        _validate(data: any): boolean;
        _absolutizeExpirationTime(expiresIn: number): Date | undefined;
        _scheduleAccessTokenRefresh(expiresIn: number, expiresAt: Date | undefined, refreshToken: string): void;
    }
}
